#include <map>
#include <string>
#include <vector>
#include "TLorentzVector.h"
#include "DataFormats/Common/interface/Wrapper.h"
#include "TClonesArray.h"

namespace {

  struct dictionary {
    std::map<std::string, std::vector<float > > dummy;
    edm::Wrapper< std::map<std::string, std::vector<float> > > dummy1;

    std::vector<std::vector<float > > dummy2;
    edm::Wrapper< std::vector<std::vector<float> > > dummy3;

    std::vector<std::vector<bool > > dummy4;
    edm::Wrapper< std::vector<std::vector<bool> > > dummy5;

    std::vector<std::vector<std::string > > dummy6;
    edm::Wrapper< std::vector<std::vector<std::string> > > dummy7;

    std::vector<TLorentzVector> dummy8;
    edm::Wrapper< std::vector<TLorentzVector> > dummy9;

    std::vector<std::vector<TLorentzVector> > dummy10;
    edm::Wrapper< std::vector<std::vector<TLorentzVector> > > dummy11;

  };

}
