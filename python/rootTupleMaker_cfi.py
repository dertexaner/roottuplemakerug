import FWCore.ParameterSet.Config as cms

rootTupleL1EGXtalClusterNoCuts = cms.EDProducer("RootTupleMaker_L1EG",
                                                        Prefix = cms.string('L1EGXtalClusterNoCuts'),
                                                        Suffix = cms.string(''),
#                                                        Pt = cms.double(5),
                                                        Pt = cms.double(-1),
                                                        BXVectorInputTag         = cms.InputTag( "" ),
                                                        EGCrystalClusterInputTag = cms.InputTag( "l1EGammaCrystalsProducer", "L1EGXtalClusterNoCuts", "RUTGERSUPGRADES" ),
                                                        EMParticlesInputTag      = cms.InputTag( "" )
)

rootTupleL1EGCollectionWithCuts = cms.EDProducer("RootTupleMaker_L1EG",
                                                        Prefix = cms.string('L1EGCollectionWithCuts'),
                                                        Suffix = cms.string(''),
#                                                        Pt = cms.double(5),
                                                        Pt = cms.double(-1),
                                                        BXVectorInputTag         = cms.InputTag( "" ),
                                                        EGCrystalClusterInputTag = cms.InputTag( "" ),
                                                        EMParticlesInputTag      = cms.InputTag( "l1EGammaCrystalsProducer", "L1EGCollectionWithCuts", "RUTGERSUPGRADES" )
)

rootTupleL1EGXtalClusterWithCuts = cms.EDProducer("RootTupleMaker_L1EG",
                                                        Prefix = cms.string('L1EGXtalClusterWithCuts'),
                                                        Suffix = cms.string(''),
#                                                        Pt = cms.double(5),
                                                        Pt = cms.double(-1),
                                                        BXVectorInputTag         = cms.InputTag( "" ),
                                                        EGCrystalClusterInputTag = cms.InputTag( "l1EGammaCrystalsProducer", "L1EGXtalClusterWithCuts", "RUTGERSUPGRADES" ),
                                                        EMParticlesInputTag      = cms.InputTag( "" )
)

rootTupleL1EGammaCollectionBXVWithCuts = cms.EDProducer("RootTupleMaker_L1EG",
                                                        Prefix = cms.string('L1EGammaCollectionBXVWithCuts'),
                                                        Suffix = cms.string(''),
#                                                        Pt = cms.double(5),
                                                        Pt = cms.double(-1),
                                                        BXVectorInputTag         = cms.InputTag( "l1EGammaCrystalsProducer", "L1EGammaCollectionBXVWithCuts", "RUTGERSUPGRADES" ),
                                                        EGCrystalClusterInputTag = cms.InputTag( "" ),
                                                        EMParticlesInputTag      = cms.InputTag( "" )
)

rootTupleL1EGXtalClusterEmulator = cms.EDProducer("RootTupleMaker_L1EG",
                                                        Prefix = cms.string('L1EGXtalClusterEmulator'),
                                                        Suffix = cms.string(''),
#                                                        Pt = cms.double(5),
                                                        Pt = cms.double(-1),
                                                        BXVectorInputTag         = cms.InputTag( "" ),
                                                        EGCrystalClusterInputTag = cms.InputTag( "L1EGammaClusterEmuProducer", "L1EGXtalClusterEmulator", "RUTGERSUPGRADES" ),
                                                        EMParticlesInputTag      = cms.InputTag( "" )
)

rootTupleL1EGammaCollectionBXVWithCutsEE = cms.EDProducer("RootTupleMaker_L1EG",
                                                        Prefix = cms.string('L1EGammaCollectionBXVWithCuts'),
                                                        Suffix = cms.string('EE'),
#                                                        Pt = cms.double(5),
                                                        Pt = cms.double(-1),
                                                        BXVectorInputTag         = cms.InputTag( "l1EGammaEEProducer", "L1EGammaCollectionBXVWithCuts", "RUTGERSUPGRADES" ),
                                                        EGCrystalClusterInputTag = cms.InputTag( "" ),
                                                        EMParticlesInputTag      = cms.InputTag( "" )
)

rootTupleL1PromptTracksNpar4 = cms.EDProducer("RootTupleMaker_L1Track",
                                              Prefix = cms.string('L1Track'),
                                              Suffix = cms.string(''),
#                                              Pt = cms.double(5),
                                              Pt = cms.double(-1),
                                              StoreStubs = cms.bool(False),
                                              L1TkNPar = cms.int32(4),
                                              L1TrackInputTag = cms.InputTag( "TTTracksFromTrackletEmulation","Level1TTTracks","RUTGERSUPGRADES")
)

rootTupleL1DispTracksNpar5 = cms.EDProducer("RootTupleMaker_L1Track",
                                                    Prefix = cms.string('L1Track'),
                                                    Suffix = cms.string(''),
#                                                    Pt = cms.double(5),
                                                    Pt = cms.double(-1),
                                                    StoreStubs = cms.bool(False),
                                                    L1TkNPar = cms.int32(5),
                                                    L1TrackInputTag = cms.InputTag( "TTTracksFromExtendedTrackletEmulation","Level1TTTracks","RUTGERSUPGRADES")
)

rootTupleL1DispTracksNpar5altname = cms.EDProducer("RootTupleMaker_L1Track",
                                                    Prefix = cms.string('L1TrackExtended'),
                                                    Suffix = cms.string(''),
#                                                    Pt = cms.double(5),
                                                    Pt = cms.double(-1),
                                                    StoreStubs = cms.bool(False),
                                                    L1TkNPar = cms.int32(5),
                                                    L1TrackInputTag = cms.InputTag( "TTTracksFromExtendedTrackletEmulation","Level1TTTracks","RUTGERSUPGRADES")
)

rootTupleGenParticles = cms.EDProducer("RootTupleMaker_GenParticles",
                                           Prefix = cms.string('GenParticle'),
                                           Suffix = cms.string(''),
                                           GenParticlesInputTag = cms.InputTag( "genParticles" )
)

rootTupleTree = cms.EDAnalyzer("RootTupleMaker_Tree",
    outputCommands = cms.untracked.vstring(
        'drop *',
        'keep *_rootTuple*_*_*',
        'drop edmHLTPathStatus_*_*_*'
    )
)
