##hs## CUSTOMIZATION >>>
#---------------------------------------------------------------------------------------------------------------------------------------------------------# 
# This is based on CMSSW_10_6_1_patch2 cms-l1t-offline:l1t-phase2-v2.23.1 of https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideL1TPhase2Instructions
#---------------------------------------------------------------------------------------------------------------------------------------------------------# 
turnEEoff=False
useExtended = False
useNominal = True
maxEvents = 10000
skipEvents=0
input = "SingleEleNoPU"
#input = "NuGun200PU"
#input = "SingleEle200PU"
#input = "MinBiasNoPU"
import FWCore.Utilities.FileUtils as FileUtils

if input == "SingleEleNoPU"  : inputMC = FileUtils.loadListFromFile('SingleE_FlatPt-2to100_PhaseIIMTDTDRAutumn18DR_NoPU_103X_upgrade2023_realistic_v2-v1.txt')
if input == "SingleEle200PU" : inputMC = FileUtils.loadListFromFile('SingleE_FlatPt-2to100_PhaseIIMTDTDRAutumn18DR_PU200_103X_upgrade2023_realistic_v2-v1.txt')
if input == "MinBiasNoPU"    : inputMC = FileUtils.loadListFromFile('MinBias_TuneCP5_14TeV-pythia8_PhaseIIMTDTDRAutumn18DR-NoPU_103X_upgrade2023_realistic_v2-v1.txt')
if input == "NuGun200PU"     : inputMC = FileUtils.loadListFromFile('NeutrinoGun_E_10GeV_PhaseIIMTDTDRAutumn18DR_PU200_103X_upgrade2023_realistic_v2-v1.txt')

##hs## CUSTOMIZATION <<<

# Auto generated configuration file
# using: 
# Revision: 1.19 
# Source: /local/reps/CMSSW/CMSSW/Configuration/Applications/python/ConfigBuilder.py,v 
# with command line options: repr --processName=REPR --python_filename=reprocess_test_10_5_0_pre1.py --no_exec -s L1 --datatier GEN-SIM-DIGI-RAW -n 2 --era Phase2 --eventcontent FEVTDEBUGHLT --filein root://cms-xrd-global.cern.ch//store/mc/PhaseIIMTDTDRAutumn18DR/DYToLL_M-50_14TeV_pythia8/FEVT/PU200_pilot_103X_upgrade2023_realistic_v2_ext4-v1/280000/FF5C31D5-D96E-5E48-B97F-61A0E00DF5C4.root --conditions 103X_upgrade2023_realistic_v2 --beamspot HLLHC14TeV --geometry Extended2023D28 --fileout file:step2_2ev_reprocess_slim.root
import FWCore.ParameterSet.Config as cms

from Configuration.StandardSequences.Eras import eras

process = cms.Process('RUTGERSUPGRADES',eras.Phase2C4_trigger) #hs#
#hs#process = cms.Process('REPR',eras.Phase2C4_trigger)
#process = cms.Process('REPR',eras.Phase2C4_timing_layer_bar)

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.Geometry.GeometryExtended2023D35Reco_cff')
process.load('Configuration.Geometry.GeometryExtended2023D35_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.SimL1Emulator_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(maxEvents)
)



# Input source
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(*inputMC), #hs#
    skipEvents=cms.untracked.uint32(skipEvents), #hs#
    #hs#fileNames = cms.untracked.vstring('root://cms-xrd-global.cern.ch//store/mc/PhaseIIMTDTDRAutumn18DR/NeutrinoGun_E_10GeV/FEVT/PU200_103X_upgrade2023_realistic_v2-v1/280000/CF7695FC-46FE-F649-9A7E-47ABE65D3861.root'),
    #fileNames = cms.untracked.vstring('root://cms-xrd-global.cern.ch//store/mc/PhaseIIMTDTDRAutumn18DR/NeutrinoGun_E_10GeV/FEVT/NoPU_103X_upgrade2023_realistic_v2_ext1-v1/60000/F620EB2E-E6E2-8343-B59E-109D2A52C764.root'),

    secondaryFileNames = cms.untracked.vstring()
)

process.options = cms.untracked.PSet(

)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('repr nevts:2'),
    name = cms.untracked.string('Applications'),
    version = cms.untracked.string('$Revision: 1.19 $')
)

# Output definition

process.FEVTDEBUGHLToutput = cms.OutputModule("PoolOutputModule",
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string('GEN-SIM-DIGI-RAW'),
        filterName = cms.untracked.string('')
    ),
    fileName = cms.untracked.string('file:step2_2ev_reprocess_slim.root'),
    outputCommands = process.FEVTDEBUGHLTEventContent.outputCommands,
    splitLevel = cms.untracked.int32(0)
)

# Additional output definition


# Other statements
from Configuration.AlCa.GlobalTag import GlobalTag
#process.GlobalTag = GlobalTag(process.GlobalTag, '103X_upgrade2023_realistic_v2', '')
#process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase2_realistic', '')

process.GlobalTag = GlobalTag(process.GlobalTag, '103X_upgrade2023_realistic_v2', '') 

process.load('SimCalorimetry.HcalTrigPrimProducers.hcaltpdigi_cff')
process.load('CalibCalorimetry.CaloTPG.CaloTPGTranscoder_cfi')

# Path and EndPath definitions
process.L1simulation_step = cms.Path(process.SimL1Emulator)
process.endjob_step = cms.EndPath(process.endOfProcess)
process.FEVTDEBUGHLToutput_step = cms.EndPath(process.FEVTDEBUGHLToutput)

##hs## CUSTOMIZATION >>>
process.load('Multilepton.RootTupleMaker.rootTupleMaker_cfi')

if useExtended and not useNominal:
    process.load("L1Trigger.TrackFindingTracklet.L1ExtendedTrackletEmulationTracks_cff")
    if turnEEoff :
        process.TracksAndClusters = cms.Path(
            process.L1ExtendedTrackletEmulationTracks*
            process.l1EGammaCrystalsProducer*
            process.L1EGammaClusterEmuProducer
        )
    else:
        process.TracksAndClusters = cms.Path(
            process.L1ExtendedTrackletEmulationTracks*
            process.l1EGammaCrystalsProducer*
            process.L1EGammaClusterEmuProducer*
            process.l1EGammaEEProducer
            )
    process.rootTupleMaker = cms.Path(
        process.rootTupleL1DispTracksNpar5*
        process.rootTupleL1EGXtalClusterNoCuts*
        process.rootTupleL1EGCollectionWithCuts*
        process.rootTupleL1EGXtalClusterWithCuts*
        process.rootTupleL1EGammaCollectionBXVWithCuts*
        process.rootTupleL1EGammaCollectionBXVWithCutsEE*
        process.rootTupleGenParticles*
        process.rootTupleTree
    )


if not useExtended and useNominal:
    process.load("L1Trigger.TrackFindingTracklet.L1TrackletEmulationTracks_cff")
    if turnEEoff :
        process.TracksAndClusters = cms.Path(
            process.L1TrackletEmulationTracks*
            process.l1EGammaCrystalsProducer*
            process.L1EGammaClusterEmuProducer
            )
    else :
        process.TracksAndClusters = cms.Path(
            process.L1TrackletEmulationTracks*
            process.l1EGammaCrystalsProducer*
            process.L1EGammaClusterEmuProducer*
            process.l1EGammaEEProducer
        )
    process.rootTupleMaker = cms.Path(
        process.rootTupleL1PromptTracksNpar4*
        process.rootTupleL1EGXtalClusterNoCuts*
        process.rootTupleL1EGCollectionWithCuts*
        process.rootTupleL1EGXtalClusterWithCuts*
        process.rootTupleL1EGammaCollectionBXVWithCuts*
        process.rootTupleL1EGammaCollectionBXVWithCutsEE*
        process.rootTupleGenParticles*
        process.rootTupleTree
    )



if not useExtended and useNominal : process.TFileService = cms.Service("TFileService", fileName = cms.string( "results_Nominal_"+input+"_"+str(skipEvents)+"_"+str(maxEvents)+".root" ) )
if useExtended and not useNominal : process.TFileService = cms.Service("TFileService", fileName = cms.string( "results_Extended_"+input+"_"+str(skipEvents)+"_"+str(maxEvents)+".root" ) )

#process.Outdump = cms.OutputModule( "PoolOutputModule",
#    fileName = cms.untracked.string( "out.root" ),
#    fastCloning = cms.untracked.bool( False ),
#    outputCommands = cms.untracked.vstring(
#                    'drop *',
#                    #'keep *',
#                    #'drop *HGCal*_*_*_*',
#                    #'drop *_*HGCal*_*_*',
#                    #'drop *_*_*HGCal*_*',
#                    #'drop *_*_*_*HGCal*'
#                    'keep *_l1EGammaEEProducer*_*_*'
#                    #'drop *',
#                    #'keep *_rootTuple*_*_*'
#                    )
#)
#process.end = cms.EndPath(process.Outdump )

process.schedule = cms.Schedule(
process.L1simulation_step,
process.TracksAndClusters,
#process.end
process.rootTupleMaker,
process.endjob_step
)

##hs## CUSTOMIZATION <<<



# Schedule definition
#process.schedule = cms.Schedule(process.L1simulation_step,process.endjob_step,process.FEVTDEBUGHLToutput_step)
#hs#process.schedule = cms.Schedule(process.L1simulation_step,process.endjob_step)

from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)


# Customisation from command line

from L1Trigger.Configuration.customiseUtils import L1TrackTriggerTracklet,configureCSCLCTAsRun2
process = L1TrackTriggerTracklet(process)
process = configureCSCLCTAsRun2(process)

# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion
