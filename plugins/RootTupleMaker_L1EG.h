#ifndef RootTupleMakerL1EG
#define RootTupleMakerL1EG

#include "FWCore/Framework/interface/EDProducer.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/VertexReco/interface/VertexFwd.h"
#include "FWCore/Utilities/interface/StreamID.h"
#include "DataFormats/Scalers/interface/L1AcceptBunchCrossing.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/VertexReco/interface/VertexFwd.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/L1Trigger/interface/EGamma.h"
#include "DataFormats/PatCandidates/interface/TriggerObjectStandAlone.h"
#include "DataFormats/PatCandidates/interface/PackedTriggerPrescales.h"
#include "DataFormats/HLTReco/interface/TriggerTypeDefs.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/Utilities/interface/transform.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "DataFormats/Phase2L1CaloTrig/interface/L1EGCrystalCluster.h"
#include "DataFormats/L1Trigger/interface/L1EmParticle.h"
#include "DataFormats/L1Trigger/interface/BXVector.h"

class RootTupleMaker_L1EG : public edm::EDProducer {
 public:
  explicit RootTupleMaker_L1EG(const edm::ParameterSet&);
 private:
  void produce( edm::Event &, const edm::EventSetup & );
  const std::string  prefix, suffix;
  const edm::EDGetTokenT<BXVector<l1t::EGamma> >               bxvectorToken_;
  const double  ptcut_;
  const edm::EDGetTokenT<l1slhc::L1EGCrystalClusterCollection> crystalClustersToken_;
  const edm::EDGetTokenT<l1extra::L1EmParticleCollection>      emParticlesToken_;
};

#endif
