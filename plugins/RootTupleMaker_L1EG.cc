#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_L1EG.h"
#include "FWCore/Framework/interface/Event.h"
#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_Tree.h"

RootTupleMaker_L1EG::RootTupleMaker_L1EG(const edm::ParameterSet& iConfig) :
  prefix                 (iConfig.getParameter<std::string>("Prefix")),
  suffix                 (iConfig.getParameter<std::string>("Suffix")),
  bxvectorToken_         (consumes<BXVector<l1t::EGamma> >(iConfig.getParameter<edm::InputTag>("BXVectorInputTag"))),
  ptcut_                 (iConfig.getParameter< double >("Pt")),
  //https://twiki.cern.ch/twiki/bin/viewauth/CMS/L1EGammaCrystals#Current_Documentation_in_Detecto
  crystalClustersToken_  (consumes<l1slhc::L1EGCrystalClusterCollection>(iConfig.getParameter<edm::InputTag>("EGCrystalClusterInputTag"))),
  emParticlesToken_      (consumes<l1extra::L1EmParticleCollection>(iConfig.getParameter<edm::InputTag>("EMParticlesInputTag")))
{
  using namespace std;
  //
  // See: https://twiki.cern.ch/twiki/bin/viewauth/CMS/L1EGammaCrystals#L1EG_Tunable_Quantities_in_the_E
  // Calibrated pT is based on aligning the pT response of the L1EG objects to the Phase-1 / Stage-2 L1EG objects. Because the Phase-1 / Stage-2 L1EG objects closely align with gen pT, this effectively calibrates the Phase-2 L1EG object to gen.
  produces <vector<float> >        ( prefix + "UncorrectedEnergy"      + suffix );
  produces <vector<float> >        ( prefix + "Energy"                 + suffix );
  produces <vector<float> >        ( prefix + "CalibratedPt"           + suffix );
  produces <vector<float> >        ( prefix + "UncorrectedPt"          + suffix );
  produces <vector<float> >        ( prefix + "Pt"                     + suffix );
  produces <vector<float> >        ( prefix + "Eta"                    + suffix );
  produces <vector<float> >        ( prefix + "Phi"                    + suffix );
  produces <vector<float> >        ( prefix + "HoE"                    + suffix );
  produces <vector<float> >        ( prefix + "Isolation"              + suffix );
  produces <vector<float> >        ( prefix + "E2x5"                   + suffix );
  produces <vector<float> >        ( prefix + "E3x5"                   + suffix );
  produces <vector<float> >        ( prefix + "E5x5"                   + suffix );
  produces <vector<float> >        ( prefix + "BremStrength"           + suffix );
  produces <vector<float> >        ( prefix + "UpperSideLobePt"        + suffix );
  produces <vector<float> >        ( prefix + "LowerSideLobePt"        + suffix );
  produces <vector<float> >        ( prefix + "CrystalCount"           + suffix );
  produces <vector<int> >          ( prefix + "PassIDLooseL1TkMatchWP" + suffix );
  produces <vector<int> >          ( prefix + "PassIDStandaloneWP"     + suffix );
  produces <vector<int> >          ( prefix + "PassIDPhotonWP80"       + suffix );
  produces <vector<int> >          ( prefix + "PassIDElectronWP98"     + suffix );
  produces <vector<int> >          ( prefix + "PassIDElectronWP90"     + suffix );
  produces <vector<int> >          ( prefix + "PassIDStage2EffMatch"   + suffix );
  produces <vector<int> >          ( prefix + "HWQual"                 + suffix );
  produces <vector<int> >          ( prefix + "HWIso"                  + suffix );
  produces <vector<int> >          ( prefix + "N"                      + suffix );
}

void RootTupleMaker_L1EG:: produce(edm::Event& iEvent, const edm::EventSetup& iSetup) {
  using namespace edm;
  using namespace std;
  auto uncorrectedenergy       = make_unique<vector<float>>();
  auto energy                  = make_unique<vector<float>>();
  auto calibratedpt            = make_unique<vector<float>>();
  auto uncorrectedpt           = make_unique<vector<float>>();
  auto pt                      = make_unique<vector<float>>();
  auto eta                     = make_unique<vector<float>>();
  auto phi                     = make_unique<vector<float>>();
  auto hoe                     = make_unique<vector<float>>();
  auto isolation               = make_unique<vector<float>>();
  auto e2x5                    = make_unique<vector<float>>();
  auto e3x5                    = make_unique<vector<float>>();
  auto e5x5                    = make_unique<vector<float>>();
  auto bremstrength            = make_unique<vector<float>>();
  auto uppersidelobept         = make_unique<vector<float>>();
  auto lowersidelobept         = make_unique<vector<float>>();
  auto crystalcount            = make_unique<vector<float>>();
  auto passidloosel1tkmatchwp  = make_unique<vector<int>>();
  auto passidstandalonewp      = make_unique<vector<int>>();
  auto passidphotonwp80        = make_unique<vector<int>>();
  auto passidelectronwp98      = make_unique<vector<int>>();
  auto passidelectronwp90      = make_unique<vector<int>>();
  auto passidstage2effmatch    = make_unique<vector<int>>();
  auto hwqual                  = make_unique<vector<int>>();
  auto hwiso                   = make_unique<vector<int>>();
  auto n                       = make_unique<vector<int>>();
  // ------------------------------------------------------------------------------------------------------------

  Handle<l1slhc::L1EGCrystalClusterCollection> crystalClustersHandle;  
  iEvent.getByToken(crystalClustersToken_,crystalClustersHandle);
  Handle<l1extra::L1EmParticleCollection> emParticlesHandle;
  iEvent.getByToken(emParticlesToken_, emParticlesHandle);
  Handle<BXVector<l1t::EGamma> > bxvectorHandle;
  iEvent.getByToken(bxvectorToken_,bxvectorHandle);

  if( bxvectorHandle.isValid() ){
    for(auto l1obj = bxvectorHandle.product()->begin(0); l1obj != bxvectorHandle.product()->end(0); l1obj++){
      //cout<<"l1obj->pt(): "<< l1obj->pt() <<" ";
      //cout<<"l1obj->eta(): "<< l1obj->eta() <<" ";
      //cout<<"l1obj->phi(): "<< l1obj->phi() <<" ";
      //cout<<"l1obj->hwIso(): "<<l1obj->hwIso()<<" ";
      //cout<<"l1obj->hwQual(): "<<l1obj->hwQual()<<endl;
      //
      if( l1obj->pt() < ptcut_ ) continue;
      pt                ->push_back( l1obj->pt() );
      eta               ->push_back( l1obj->eta() );
      phi               ->push_back( l1obj->phi() );
      energy            ->push_back( l1obj->energy() );
      hwqual            ->push_back( l1obj->hwQual() );
      hwiso             ->push_back( l1obj->hwIso() );
    }
  }

  if( emParticlesHandle.isValid() ){
    //
    l1extra::L1EmParticleCollection emParticles;
    emParticles = (*emParticlesHandle.product());
    //
    n ->push_back( (int)emParticles.size() );
    //
    for( const auto& particle : emParticles ){
      if( particle.pt() < ptcut_ ) continue;
      energy            ->push_back( particle.energy() );
      pt                ->push_back( particle.pt() );
      eta               ->push_back( particle.eta() );
      phi               ->push_back( particle.phi() );
    }
  }

  if( crystalClustersHandle.isValid() ){
    //
    l1slhc::L1EGCrystalClusterCollection crystalClusters;
    crystalClusters = (*crystalClustersHandle.product());
    //
    n ->push_back( (int)crystalClusters.size() );
    //
    for(const auto& cluster : crystalClusters)
      {
	if( cluster.pt() < ptcut_ ) continue;
	// see :https://twiki.cern.ch/twiki/bin/view/CMS/L1EGammaCrystals#L1EG_Cluster_Variables
	//	  cluster.GetExperimentalParam("preCalibratedPt")	
	uncorrectedenergy      ->push_back( cluster.GetExperimentalParam("uncorrectedE") );
	energy                 ->push_back( cluster.energy() );
	calibratedpt           ->push_back( cluster.calibratedPt() );
	uncorrectedpt          ->push_back( cluster.GetExperimentalParam("uncorrectedPt") );
	pt                     ->push_back( cluster.pt() );
	eta                    ->push_back( cluster.eta() );
	phi                    ->push_back( cluster.phi() );
	hoe                    ->push_back( cluster.hovere() );
	isolation              ->push_back( cluster.isolation() );
	e2x5                   ->push_back( cluster.GetExperimentalParam("E2x5") );
	e3x5                   ->push_back( cluster.GetExperimentalParam("E3x5") );
	e5x5                   ->push_back( cluster.GetExperimentalParam("E5x5") );
	bremstrength           ->push_back( cluster.bremStrength() );
	uppersidelobept        ->push_back( cluster.GetExperimentalParam("upperSideLobePt") );
	lowersidelobept        ->push_back( cluster.GetExperimentalParam("lowerSideLobePt") );
	crystalcount           ->push_back( cluster.GetExperimentalParam("crystalCount") );
	passidloosel1tkmatchwp ->push_back( cluster.looseL1TkMatchWP() );
	passidstandalonewp     ->push_back( cluster.standaloneWP() );
	passidphotonwp80       ->push_back( cluster.photonWP80() );
	passidelectronwp98     ->push_back( cluster.electronWP98() );
	passidelectronwp90     ->push_back( cluster.electronWP90() );
	passidstage2effmatch   ->push_back( cluster.stage2effMatch() );
      }
  }

  // ------------------------------------------------------------------------------------------------------------
  iEvent.put( move( uncorrectedenergy ),       prefix + "UncorrectedEnergy"       + suffix );
  iEvent.put( move( energy ),                  prefix + "Energy"                  + suffix );
  iEvent.put( move( calibratedpt ),            prefix + "CalibratedPt"            + suffix );
  iEvent.put( move( uncorrectedpt ),           prefix + "UncorrectedPt"           + suffix );
  iEvent.put( move( pt ),                      prefix + "Pt"                      + suffix );
  iEvent.put( move( eta ),                     prefix + "Eta"                     + suffix );
  iEvent.put( move( phi ),                     prefix + "Phi"                     + suffix );
  iEvent.put( move( hoe ),                     prefix + "HoE"                     + suffix );
  iEvent.put( move( isolation ),               prefix + "Isolation"               + suffix );
  iEvent.put( move( e2x5 ),                    prefix + "E2x5"                    + suffix );
  iEvent.put( move( e3x5 ),                    prefix + "E3x5"                    + suffix );
  iEvent.put( move( e5x5 ),                    prefix + "E5x5"                    + suffix );
  iEvent.put( move( bremstrength ),            prefix + "BremStrength"            + suffix );
  iEvent.put( move( uppersidelobept ),         prefix + "UpperSideLobePt"         + suffix );
  iEvent.put( move( lowersidelobept ),         prefix + "LowerSideLobePt"         + suffix );
  iEvent.put( move( crystalcount ),            prefix + "CrystalCount"            + suffix );
  iEvent.put( move( passidloosel1tkmatchwp ),  prefix + "PassIDLooseL1TkMatchWP"  + suffix );
  iEvent.put( move( passidstandalonewp ),      prefix + "PassIDStandaloneWP"      + suffix );
  iEvent.put( move( passidphotonwp80 ),        prefix + "PassIDPhotonWP80"        + suffix );
  iEvent.put( move( passidelectronwp98 ),      prefix + "PassIDElectronWP98"      + suffix );
  iEvent.put( move( passidelectronwp90 ),      prefix + "PassIDElectronWP90"      + suffix );
  iEvent.put( move( passidstage2effmatch ),    prefix + "PassIDStage2EffMatch"    + suffix );
  iEvent.put( move( hwqual ),                  prefix + "HWQual"                  + suffix );
  iEvent.put( move( hwiso ),                   prefix + "HWIso"                   + suffix );
  iEvent.put( move( n ),                       prefix + "N"                       + suffix );
}
