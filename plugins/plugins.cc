#include "FWCore/PluginManager/interface/ModuleDef.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_Tree.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_Event.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_SimplifiedEvent.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_METFilters.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_Electrons.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_Muons.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_Taus.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_Jets.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_MET.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_GenEventInfo.h"
#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_GenParticles.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_GenMET.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_Photons.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_Tracks.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_TriggerObjects.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_TriggerMatching.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_TriggerResults.h"
#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_L1EG.h"
#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_L1Track.h"
//#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_TheoryVariables.h"

DEFINE_FWK_MODULE(RootTupleMaker_Tree);
//DEFINE_FWK_MODULE(RootTupleMaker_Event);
//DEFINE_FWK_MODULE(RootTupleMaker_SimplifiedEvent);
//DEFINE_FWK_MODULE(RootTupleMaker_METFilters);
//DEFINE_FWK_MODULE(RootTupleMaker_Jets);
//DEFINE_FWK_MODULE(RootTupleMaker_Electrons);
//DEFINE_FWK_MODULE(RootTupleMaker_Taus);
//DEFINE_FWK_MODULE(RootTupleMaker_MET);
//DEFINE_FWK_MODULE(RootTupleMaker_Muons);
//DEFINE_FWK_MODULE(RootTupleMaker_GenEventInfo);
DEFINE_FWK_MODULE(RootTupleMaker_GenParticles);
//DEFINE_FWK_MODULE(RootTupleMaker_GenMET);
//DEFINE_FWK_MODULE(RootTupleMaker_Photons);
//DEFINE_FWK_MODULE(RootTupleMaker_Tracks);
//DEFINE_FWK_MODULE(RootTupleMaker_TriggerObjects);
//DEFINE_FWK_MODULE(RootTupleMaker_TriggerMatching);
//DEFINE_FWK_MODULE(RootTupleMaker_TriggerResults);
DEFINE_FWK_MODULE(RootTupleMaker_L1EG);
DEFINE_FWK_MODULE(RootTupleMaker_L1Track);
//DEFINE_FWK_MODULE(RootTupleMaker_TheoryVariables);
