#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_GenParticles.h"
#include "FWCore/Framework/interface/Event.h"
#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_Tree.h"

RootTupleMaker_GenParticles::RootTupleMaker_GenParticles(const edm::ParameterSet& iConfig) :
  prefix                 (iConfig.getParameter<std::string>("Prefix")),
  suffix                 (iConfig.getParameter<std::string>("Suffix")),
  GenParticlesInputToken_(consumes<vector<reco::GenParticle> >(iConfig.getParameter<edm::InputTag>("GenParticlesInputTag")))
{
  using namespace std;
  //
  produces <vector<int> >          ( prefix + "N"                      + suffix );
  produces <vector<float> >        ( prefix + "Pt"                     + suffix );
  produces <vector<float> >        ( prefix + "Eta"                    + suffix );
  produces <vector<float> >        ( prefix + "Phi"                    + suffix );
  produces <vector<float> >        ( prefix + "Mass"                   + suffix );
  produces <vector<int> >          ( prefix + "Status"                 + suffix );
  produces <vector<int> >          ( prefix + "Charge"                 + suffix );
  produces <vector<int> >          ( prefix + "PDGID"                  + suffix );
  produces <vector<float> >        ( prefix + "X0"                     + suffix );
  produces <vector<float> >        ( prefix + "Y0"                     + suffix );
  produces <vector<float> >        ( prefix + "Z0"                     + suffix );
  produces <vector<float> >        ( prefix + "D0"                     + suffix );
}

void RootTupleMaker_GenParticles::produce(edm::Event& iEvent, const edm::EventSetup& iSetup) {
  using namespace edm;
  using namespace std;
  auto n                       = make_unique<vector<int>>();
  auto pt                      = make_unique<vector<float>>();
  auto eta                     = make_unique<vector<float>>();
  auto phi                     = make_unique<vector<float>>();
  auto mass                    = make_unique<vector<float>>();
  auto status                  = make_unique<vector<int>>();
  auto charge                  = make_unique<vector<int>>();
  auto pdgid                   = make_unique<vector<int>>();
  auto x0                      = make_unique<vector<float>>();
  auto y0                      = make_unique<vector<float>>();
  auto z0                      = make_unique<vector<float>>();
  auto d0                      = make_unique<vector<float>>();
  // ------------------------------------------------------------------------------------------------------------

  Handle<vector<reco::GenParticle> > genparticles;
  iEvent.getByToken(GenParticlesInputToken_, genparticles);
  //
  if( genparticles.isValid() ){
    //
    std::vector< reco::GenParticle >::const_iterator particle;
    for ( particle = genparticles->begin(); particle != genparticles->end(); particle++ ) {
      pt     ->push_back( particle->pt() );
      eta    ->push_back( particle->eta() );
      phi    ->push_back( particle->phi() );
      mass   ->push_back( particle->mass() );
      status ->push_back( particle->status() );
      charge ->push_back( particle->charge() );
      pdgid  ->push_back( particle->pdgId() );
      x0      ->push_back( particle->vx() );
      y0      ->push_back( particle->vy() );
      z0      ->push_back( particle->vz() );
      d0      ->push_back( -particle->vx()*TMath::Sin(particle->phi()) + particle->vy()*TMath::Cos(particle->phi()) );
    }//end loop over genparticles
  
    n->push_back( (int)genparticles->size() );
  }


  // ------------------------------------------------------------------------------------------------------------
  iEvent.put( move( n ),                       prefix + "N"                       + suffix );
  iEvent.put( move( pt ),                      prefix + "Pt"                      + suffix );
  iEvent.put( move( eta ),                     prefix + "Eta"                     + suffix );
  iEvent.put( move( phi ),                     prefix + "Phi"                     + suffix );
  iEvent.put( move( mass ),                    prefix + "Mass"                    + suffix );
  iEvent.put( move( status ),                  prefix + "Status"                  + suffix );
  iEvent.put( move( charge ),                  prefix + "Charge"                  + suffix );
  iEvent.put( move( pdgid ),                   prefix + "PDGID"                   + suffix );
  iEvent.put( move( x0 ),                      prefix + "X0"                      + suffix );
  iEvent.put( move( y0 ),                      prefix + "Y0"                      + suffix );
  iEvent.put( move( z0 ),                      prefix + "Z0"                      + suffix );
  iEvent.put( move( d0 ),                      prefix + "D0"                      + suffix );
}
