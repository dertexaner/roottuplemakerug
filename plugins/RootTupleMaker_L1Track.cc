#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_L1Track.h"
#include "FWCore/Framework/interface/Event.h"
#include "Multilepton/RootTupleMaker/plugins/RootTupleMaker_Tree.h"

RootTupleMaker_L1Track::RootTupleMaker_L1Track(const edm::ParameterSet& iConfig) :
  prefix                 (iConfig.getParameter<std::string>("Prefix")),
  suffix                 (iConfig.getParameter<std::string>("Suffix")),
  L1TkNPar_              (iConfig.getParameter< int >("L1TkNPar")),
  ptcut_                 (iConfig.getParameter< double >("Pt")),
  storeStubs_            (iConfig.getParameter< bool >("StoreStubs")),
  L1TrackInputToken_     (consumes< std::vector< TTTrack< Ref_Phase2TrackerDigi_ > > >(iConfig.getParameter<edm::InputTag>("L1TrackInputTag")))
{
  using namespace std;
  //
  produces <vector<float> >        ( prefix + "Pt"                     + suffix );
  produces <vector<float> >        ( prefix + "Eta"                    + suffix );
  produces <vector<float> >        ( prefix + "Phi"                    + suffix );
  produces <vector<int> >          ( prefix + "Charge"                 + suffix );
  produces <vector<float> >        ( prefix + "RInv"                   + suffix );
  produces <vector<float> >        ( prefix + "Chi2"                   + suffix );
  produces <vector<float> >        ( prefix + "Chi2Red"                + suffix );
  produces <vector<float> >        ( prefix + "X0"                     + suffix );
  produces <vector<float> >        ( prefix + "Y0"                     + suffix );
  produces <vector<float> >        ( prefix + "Z0"                     + suffix );
  produces <vector<float> >        ( prefix + "D0"                     + suffix );
  produces <vector<int> >          ( prefix + "BarrelLayerHits"        + suffix );
  produces <vector<int> >          ( prefix + "EndcapDiskHits"         + suffix );
  produces <vector<int> >          ( prefix + "Seed"                   + suffix );
  produces <vector<int> >          ( prefix + "N"                      + suffix );
  //
  produces <vector<int> >          ( prefix + "StubN"                  + suffix );
  //
  if( storeStubs_ ){
    produces <vector<float> >        ( prefix + "BarrelStub1X"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub1Y"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub1Z"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub1Eta"               + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub1Phi"               + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub2X"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub2Y"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub2Z"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub2Eta"               + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub2Phi"               + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub3X"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub3Y"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub3Z"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub3Eta"               + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub3Phi"               + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub4X"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub4Y"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub4Z"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub4Eta"               + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub4Phi"               + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub5X"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub5Y"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub5Z"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub5Eta"               + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub5Phi"               + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub6X"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub6Y"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub6Z"                 + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub6Eta"               + suffix );
    produces <vector<float> >        ( prefix + "BarrelStub6Phi"               + suffix );
    //
    produces <vector<float> >        ( prefix + "EndcapStub1X"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub1Y"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub1Z"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub1Eta"               + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub1Phi"               + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub2X"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub2Y"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub2Z"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub2Eta"               + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub2Phi"               + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub3X"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub3Y"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub3Z"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub3Eta"               + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub3Phi"               + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub4X"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub4Y"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub4Z"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub4Eta"               + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub4Phi"               + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub5X"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub5Y"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub5Z"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub5Eta"               + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub5Phi"               + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub6X"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub6Y"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub6Z"                 + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub6Eta"               + suffix );
    produces <vector<float> >        ( prefix + "EndcapStub6Phi"               + suffix );
  }
}

void RootTupleMaker_L1Track:: produce(edm::Event& iEvent, const edm::EventSetup& iSetup) {
  using namespace edm;
  using namespace std;
  auto pt                      = make_unique<vector<float>>();
  auto eta                     = make_unique<vector<float>>();
  auto phi                     = make_unique<vector<float>>();
  auto charge                  = make_unique<vector<int>>();
  auto rinv                    = make_unique<vector<float>>();
  auto chi2                    = make_unique<vector<float>>();
  auto chi2red                 = make_unique<vector<float>>();
  auto x0                      = make_unique<vector<float>>();
  auto y0                      = make_unique<vector<float>>();
  auto z0                      = make_unique<vector<float>>();
  auto d0                      = make_unique<vector<float>>();
  auto barrellayerhits         = make_unique<vector<int>>();
  auto endcapdiskhits          = make_unique<vector<int>>();
  auto seed                    = make_unique<vector<int>>();
  auto n                       = make_unique<vector<int>>();
  //
  auto stubn                   = make_unique<vector<int>>();
  //
  auto barrelstub1x                  = make_unique<vector<float>>();
  auto barrelstub1y                  = make_unique<vector<float>>();
  auto barrelstub1z                  = make_unique<vector<float>>();
  auto barrelstub1eta                = make_unique<vector<float>>();
  auto barrelstub1phi                = make_unique<vector<float>>();
  auto barrelstub2x                  = make_unique<vector<float>>();
  auto barrelstub2y                  = make_unique<vector<float>>();
  auto barrelstub2z                  = make_unique<vector<float>>();
  auto barrelstub2eta                = make_unique<vector<float>>();
  auto barrelstub2phi                = make_unique<vector<float>>();
  auto barrelstub3x                  = make_unique<vector<float>>();
  auto barrelstub3y                  = make_unique<vector<float>>();
  auto barrelstub3z                  = make_unique<vector<float>>();
  auto barrelstub3eta                = make_unique<vector<float>>();
  auto barrelstub3phi                = make_unique<vector<float>>();
  auto barrelstub4x                  = make_unique<vector<float>>();
  auto barrelstub4y                  = make_unique<vector<float>>();
  auto barrelstub4z                  = make_unique<vector<float>>();
  auto barrelstub4eta                = make_unique<vector<float>>();
  auto barrelstub4phi                = make_unique<vector<float>>();
  auto barrelstub5x                  = make_unique<vector<float>>();
  auto barrelstub5y                  = make_unique<vector<float>>();
  auto barrelstub5z                  = make_unique<vector<float>>();
  auto barrelstub5eta                = make_unique<vector<float>>();
  auto barrelstub5phi                = make_unique<vector<float>>();
  auto barrelstub6x                  = make_unique<vector<float>>();
  auto barrelstub6y                  = make_unique<vector<float>>();
  auto barrelstub6z                  = make_unique<vector<float>>();
  auto barrelstub6eta                = make_unique<vector<float>>();
  auto barrelstub6phi                = make_unique<vector<float>>();
  //
  auto endcapstub1x                  = make_unique<vector<float>>();
  auto endcapstub1y                  = make_unique<vector<float>>();
  auto endcapstub1z                  = make_unique<vector<float>>();
  auto endcapstub1eta                = make_unique<vector<float>>();
  auto endcapstub1phi                = make_unique<vector<float>>();
  auto endcapstub2x                  = make_unique<vector<float>>();
  auto endcapstub2y                  = make_unique<vector<float>>();
  auto endcapstub2z                  = make_unique<vector<float>>();
  auto endcapstub2eta                = make_unique<vector<float>>();
  auto endcapstub2phi                = make_unique<vector<float>>();
  auto endcapstub3x                  = make_unique<vector<float>>();
  auto endcapstub3y                  = make_unique<vector<float>>();
  auto endcapstub3z                  = make_unique<vector<float>>();
  auto endcapstub3eta                = make_unique<vector<float>>();
  auto endcapstub3phi                = make_unique<vector<float>>();
  auto endcapstub4x                  = make_unique<vector<float>>();
  auto endcapstub4y                  = make_unique<vector<float>>();
  auto endcapstub4z                  = make_unique<vector<float>>();
  auto endcapstub4eta                = make_unique<vector<float>>();
  auto endcapstub4phi                = make_unique<vector<float>>();
  auto endcapstub5x                  = make_unique<vector<float>>();
  auto endcapstub5y                  = make_unique<vector<float>>();
  auto endcapstub5z                  = make_unique<vector<float>>();
  auto endcapstub5eta                = make_unique<vector<float>>();
  auto endcapstub5phi                = make_unique<vector<float>>();
  auto endcapstub6x                  = make_unique<vector<float>>();
  auto endcapstub6y                  = make_unique<vector<float>>();
  auto endcapstub6z                  = make_unique<vector<float>>();
  auto endcapstub6eta                = make_unique<vector<float>>();
  auto endcapstub6phi                = make_unique<vector<float>>();
  
  // ------------------------------------------------------------------------------------------------------------

  Handle<vector< TTTrack< Ref_Phase2TrackerDigi_ > > > L1Track;
  iEvent.getByToken(L1TrackInputToken_, L1Track);
  //
  edm::ESHandle<TrackerGeometry> geometryHandle;
  iSetup.get<TrackerDigiGeometryRecord>().get(geometryHandle);
  edm::ESHandle<TrackerTopology> tTopoHandle;
  iSetup.get<TrackerTopologyRcd>().get(tTopoHandle);
  edm::ESHandle<TrackerGeometry> tGeomHandle;
  iSetup.get<TrackerDigiGeometryRecord>().get(tGeomHandle);
  const TrackerTopology* const tTopo = tTopoHandle.product();
  const TrackerGeometry* const theTrackerGeom = tGeomHandle.product();


  if( L1Track.isValid() ){
    //
    std::vector< TTTrack< Ref_Phase2TrackerDigi_ > >::const_iterator iterL1Track;
    for ( iterL1Track = L1Track->begin(); iterL1Track != L1Track->end(); iterL1Track++ ) {
      if( iterL1Track->getMomentum(L1TkNPar_).perp() < ptcut_ ) continue;
      pt      ->push_back( iterL1Track->getMomentum(L1TkNPar_).perp()  );
      eta     ->push_back( iterL1Track->getMomentum(L1TkNPar_).eta()   );
      phi     ->push_back( iterL1Track->getMomentum(L1TkNPar_).phi()   );
      charge  ->push_back( iterL1Track->getRInv(L1TkNPar_) > 0.0 ? 1 : -1 );
      rinv    ->push_back( iterL1Track->getRInv(L1TkNPar_) );
      chi2    ->push_back( iterL1Track->getChi2(L1TkNPar_) );
      chi2red ->push_back( iterL1Track->getChi2Red(L1TkNPar_) );
      x0      ->push_back( iterL1Track->getPOCA(L1TkNPar_).x() );
      y0      ->push_back( iterL1Track->getPOCA(L1TkNPar_).y() );
      z0      ->push_back( iterL1Track->getPOCA(L1TkNPar_).z() );
      d0      ->push_back( -iterL1Track->getPOCA(L1TkNPar_).x()*TMath::Sin(iterL1Track->getMomentum(L1TkNPar_).phi()) + iterL1Track->getPOCA(L1TkNPar_).y()*TMath::Cos(iterL1Track->getMomentum(L1TkNPar_).phi()) );
      seed    ->push_back( (int)iterL1Track->getWedge() );
      
      // ------------------------------------
      float bx_[6] = {9999.,9999.,9999.,9999.,9999.,9999.};
      float by_[6] = {9999.,9999.,9999.,9999.,9999.,9999.};
      float bz_[6] = {9999.,9999.,9999.,9999.,9999.,9999.};
      float beta_[6] = {9999.,9999.,9999.,9999.,9999.,9999.};
      float bphi_[6] = {9999.,9999.,9999.,9999.,9999.,9999.};
      float ex_[6] = {9999.,9999.,9999.,9999.,9999.,9999.};
      float ey_[6] = {9999.,9999.,9999.,9999.,9999.,9999.};
      float ez_[6] = {9999.,9999.,9999.,9999.,9999.,9999.};
      float eeta_[6] = {9999.,9999.,9999.,9999.,9999.,9999.};
      float ephi_[6] = {9999.,9999.,9999.,9999.,9999.,9999.};
      //
      int tmp_trk_lhits = 0;
      int tmp_trk_dhits = 0;
      //
      // loop over each track "stubs"
      std::vector< edm::Ref< edmNew::DetSetVector< TTStub< Ref_Phase2TrackerDigi_ > >, TTStub< Ref_Phase2TrackerDigi_ > > > stubRefs = iterL1Track->getStubRefs();
      //
      //cout<<"loop over each track stubs: "<<(int)iterL1Track->getStubRefs().size()<<endl;
      for (int is=0; is<(int)iterL1Track->getStubRefs().size(); is++) {
	//detID of stub
	DetId detIdStub = theTrackerGeom->idToDet( (stubRefs.at(is)->getClusterRef(0))->getDetId() )->geographicalId();
	MeasurementPoint coords = stubRefs.at(is)->getClusterRef(0)->findAverageLocalCoordinatesCentered();
	const GeomDet* theGeomDet = theTrackerGeom->idToDet(detIdStub);
	Global3DPoint posStub = theGeomDet->surface().toGlobal( theGeomDet->topology().localPosition(coords) );
	int layer=-999999;
	/**/
	if ( detIdStub.subdetId()==StripSubdetector::TOB ) {
	  // normally this is "Tracker Outer Barrel"
	  // Phase 2 Outer Tracker Barrel
	  layer  = static_cast<int>(tTopo->layer(detIdStub));
	  //	  cout << "   stub in layer " << layer << " at position x y z = " << posStub.x() << " " << posStub.y() << " " << posStub.z() << endl;	  
	  tmp_trk_lhits+=pow(10,layer-1); //barrel layer hits
	}
	else if ( detIdStub.subdetId()==StripSubdetector::TID ) {
	  // normally this is "Tracker Inner Disc "
	  // Phase 2 Outer Tracker Endcap
	  layer  = static_cast<int>(tTopo->layer(detIdStub));
	  //	  cout << "   stub in disk " << layer << " at position x y z = " << posStub.x() << " " << posStub.y() << " " << posStub.z() << endl;
	  tmp_trk_dhits+=pow(10,layer-1); //endcap disk hits
	}
	/**/
	//layer  = static_cast<int>(tTopo->layer(detIdStub));
	//cout<<"   stub in eta/phi/layer/subdetID : "<<posStub.eta()<<"/"<<posStub.phi()<<"/"<<layer<<"/"<<detIdStub.subdetId()<<endl;
	//
	if( storeStubs_ ){
	  if ( detIdStub.subdetId()==StripSubdetector::TOB ) {
	    bx_[layer-1]   = posStub.x();
	    by_[layer-1]   = posStub.y();
	    bz_[layer-1]   = posStub.z();
	    beta_[layer-1] = posStub.eta();
	    bphi_[layer-1] = posStub.phi();
	  }
	  //
	  if ( detIdStub.subdetId()==StripSubdetector::TID ) {
	    ex_[layer-1]   = posStub.x();
	    ey_[layer-1]   = posStub.y();
	    ez_[layer-1]   = posStub.z();
	    eeta_[layer-1] = posStub.eta();
	    ephi_[layer-1] = posStub.phi();
	  }
	  //
	}
      }//end loop over stubs
      // ------------------------------------

      if( storeStubs_ ){
	barrelstub1x     ->push_back( bx_[0]   );
	barrelstub1y     ->push_back( by_[0]   );
	barrelstub1z     ->push_back( bz_[0]   );
	barrelstub1eta   ->push_back( beta_[0] );
	barrelstub1phi   ->push_back( bphi_[0] );
	barrelstub2x     ->push_back( bx_[1]   );
	barrelstub2y     ->push_back( by_[1]   );
	barrelstub2z     ->push_back( bz_[1]   );
	barrelstub2eta   ->push_back( beta_[1] );
	barrelstub2phi   ->push_back( bphi_[1] );
	barrelstub3x     ->push_back( bx_[2]   );
	barrelstub3y     ->push_back( by_[2]   );
	barrelstub3z     ->push_back( bz_[2]   );
	barrelstub3eta   ->push_back( beta_[2] );
	barrelstub3phi   ->push_back( bphi_[2] );
	barrelstub4x     ->push_back( bx_[3]   );
	barrelstub4y     ->push_back( by_[3]   );
	barrelstub4z     ->push_back( bz_[3]   );
	barrelstub4eta   ->push_back( beta_[3] );
	barrelstub4phi   ->push_back( bphi_[3] );
	barrelstub5x     ->push_back( bx_[4]   );
	barrelstub5y     ->push_back( by_[4]   );
	barrelstub5z     ->push_back( bz_[4]   );
	barrelstub5eta   ->push_back( beta_[4] );
	barrelstub5phi   ->push_back( bphi_[4] );
	barrelstub6x     ->push_back( bx_[5]   );
	barrelstub6y     ->push_back( by_[5]   );
	barrelstub6z     ->push_back( bz_[5]   );
	barrelstub6eta   ->push_back( beta_[5] );
	barrelstub6phi   ->push_back( bphi_[5] );
	endcapstub1x     ->push_back( ex_[0]   );
	endcapstub1y     ->push_back( ey_[0]   );
	endcapstub1z     ->push_back( ez_[0]   );
	endcapstub1eta   ->push_back( eeta_[0] );
	endcapstub1phi   ->push_back( ephi_[0] );
	endcapstub2x     ->push_back( ex_[1]   );
	endcapstub2y     ->push_back( ey_[1]   );
	endcapstub2z     ->push_back( ez_[1]   );
	endcapstub2eta   ->push_back( eeta_[1] );
	endcapstub2phi   ->push_back( ephi_[1] );
	endcapstub3x     ->push_back( ex_[2]   );
	endcapstub3y     ->push_back( ey_[2]   );
	endcapstub3z     ->push_back( ez_[2]   );
	endcapstub3eta   ->push_back( eeta_[2] );
	endcapstub3phi   ->push_back( ephi_[2] );
	endcapstub4x     ->push_back( ex_[3]   );
	endcapstub4y     ->push_back( ey_[3]   );
	endcapstub4z     ->push_back( ez_[3]   );
	endcapstub4eta   ->push_back( eeta_[3] );
	endcapstub4phi   ->push_back( ephi_[3] );
	endcapstub5x     ->push_back( ex_[4]   );
	endcapstub5y     ->push_back( ey_[4]   );
	endcapstub5z     ->push_back( ez_[4]   );
	endcapstub5eta   ->push_back( eeta_[4] );
	endcapstub5phi   ->push_back( ephi_[4] );
	endcapstub6x     ->push_back( ex_[5]   );
	endcapstub6y     ->push_back( ey_[5]   );
	endcapstub6z     ->push_back( ez_[5]   );
	endcapstub6eta   ->push_back( eeta_[5] );
	endcapstub6phi   ->push_back( ephi_[5] );
      }
      //
      stubn      ->push_back( (int)(iterL1Track->getStubRefs().size()) );
      //
      barrellayerhits ->push_back( tmp_trk_lhits );
      endcapdiskhits  ->push_back( tmp_trk_dhits );
      //      
    }//end loop over tracks
  
  }//L1Track.isValid()


  // ------------------------------------------------------------------------------------------------------------
  iEvent.put( move( pt ),                      prefix + "Pt"                      + suffix );
  iEvent.put( move( eta ),                     prefix + "Eta"                     + suffix );
  iEvent.put( move( phi ),                     prefix + "Phi"                     + suffix );
  iEvent.put( move( charge ),                  prefix + "Charge"                  + suffix );
  iEvent.put( move( rinv ),                    prefix + "RInv"                    + suffix );
  iEvent.put( move( chi2 ),                    prefix + "Chi2"                    + suffix );
  iEvent.put( move( chi2red ),                 prefix + "Chi2Red"                 + suffix );
  iEvent.put( move( x0 ),                      prefix + "X0"                      + suffix );
  iEvent.put( move( y0 ),                      prefix + "Y0"                      + suffix );
  iEvent.put( move( z0 ),                      prefix + "Z0"                      + suffix );
  iEvent.put( move( d0 ),                      prefix + "D0"                      + suffix );
  iEvent.put( move( barrellayerhits ),         prefix + "BarrelLayerHits"         + suffix );
  iEvent.put( move( endcapdiskhits ),          prefix + "EndcapDiskHits"          + suffix );
  iEvent.put( move( seed ),                    prefix + "Seed"                    + suffix );
  iEvent.put( move( n ),                       prefix + "N"                       + suffix );
  //
  iEvent.put( move( stubn ),                   prefix + "StubN"                   + suffix );
  //
  if( storeStubs_ ){
    iEvent.put( move( barrelstub1x ),                  prefix + "BarrelStub1X"                  + suffix );
    iEvent.put( move( barrelstub1y ),                  prefix + "BarrelStub1Y"                  + suffix );
    iEvent.put( move( barrelstub1z ),                  prefix + "BarrelStub1Z"                  + suffix );
    iEvent.put( move( barrelstub1eta ),                prefix + "BarrelStub1Eta"                + suffix );
    iEvent.put( move( barrelstub1phi ),                prefix + "BarrelStub1Phi"                + suffix );
    iEvent.put( move( barrelstub2x ),                  prefix + "BarrelStub2X"                  + suffix );
    iEvent.put( move( barrelstub2y ),                  prefix + "BarrelStub2Y"                  + suffix );
    iEvent.put( move( barrelstub2z ),                  prefix + "BarrelStub2Z"                  + suffix );
    iEvent.put( move( barrelstub2eta ),                prefix + "BarrelStub2Eta"                + suffix );
    iEvent.put( move( barrelstub2phi ),                prefix + "BarrelStub2Phi"                + suffix );
    iEvent.put( move( barrelstub3x ),                  prefix + "BarrelStub3X"                  + suffix );
    iEvent.put( move( barrelstub3y ),                  prefix + "BarrelStub3Y"                  + suffix );
    iEvent.put( move( barrelstub3z ),                  prefix + "BarrelStub3Z"                  + suffix );
    iEvent.put( move( barrelstub3eta ),                prefix + "BarrelStub3Eta"                + suffix );
    iEvent.put( move( barrelstub3phi ),                prefix + "BarrelStub3Phi"                + suffix );
    iEvent.put( move( barrelstub4x ),                  prefix + "BarrelStub4X"                  + suffix );
    iEvent.put( move( barrelstub4y ),                  prefix + "BarrelStub4Y"                  + suffix );
    iEvent.put( move( barrelstub4z ),                  prefix + "BarrelStub4Z"                  + suffix );
    iEvent.put( move( barrelstub4eta ),                prefix + "BarrelStub4Eta"                + suffix );
    iEvent.put( move( barrelstub4phi ),                prefix + "BarrelStub4Phi"                + suffix );
    iEvent.put( move( barrelstub5x ),                  prefix + "BarrelStub5X"                  + suffix );
    iEvent.put( move( barrelstub5y ),                  prefix + "BarrelStub5Y"                  + suffix );
    iEvent.put( move( barrelstub5z ),                  prefix + "BarrelStub5Z"                  + suffix );
    iEvent.put( move( barrelstub5eta ),                prefix + "BarrelStub5Eta"                + suffix );
    iEvent.put( move( barrelstub5phi ),                prefix + "BarrelStub5Phi"                + suffix );
    iEvent.put( move( barrelstub6x ),                  prefix + "BarrelStub6X"                  + suffix );
    iEvent.put( move( barrelstub6y ),                  prefix + "BarrelStub6Y"                  + suffix );
    iEvent.put( move( barrelstub6z ),                  prefix + "BarrelStub6Z"                  + suffix );
    iEvent.put( move( barrelstub6eta ),                prefix + "BarrelStub6Eta"                + suffix );
    iEvent.put( move( barrelstub6phi ),                prefix + "BarrelStub6Phi"                + suffix );
    // 
    iEvent.put( move( endcapstub1x ),                  prefix + "EndcapStub1X"                  + suffix );
    iEvent.put( move( endcapstub1y ),                  prefix + "EndcapStub1Y"                  + suffix );
    iEvent.put( move( endcapstub1z ),                  prefix + "EndcapStub1Z"                  + suffix );
    iEvent.put( move( endcapstub1eta ),                prefix + "EndcapStub1Eta"                + suffix );
    iEvent.put( move( endcapstub1phi ),                prefix + "EndcapStub1Phi"                + suffix );
    iEvent.put( move( endcapstub2x ),                  prefix + "EndcapStub2X"                  + suffix );
    iEvent.put( move( endcapstub2y ),                  prefix + "EndcapStub2Y"                  + suffix );
    iEvent.put( move( endcapstub2z ),                  prefix + "EndcapStub2Z"                  + suffix );
    iEvent.put( move( endcapstub2eta ),                prefix + "EndcapStub2Eta"                + suffix );
    iEvent.put( move( endcapstub2phi ),                prefix + "EndcapStub2Phi"                + suffix );
    iEvent.put( move( endcapstub3x ),                  prefix + "EndcapStub3X"                  + suffix );
    iEvent.put( move( endcapstub3y ),                  prefix + "EndcapStub3Y"                  + suffix );
    iEvent.put( move( endcapstub3z ),                  prefix + "EndcapStub3Z"                  + suffix );
    iEvent.put( move( endcapstub3eta ),                prefix + "EndcapStub3Eta"                + suffix );
    iEvent.put( move( endcapstub3phi ),                prefix + "EndcapStub3Phi"                + suffix );
    iEvent.put( move( endcapstub4x ),                  prefix + "EndcapStub4X"                  + suffix );
    iEvent.put( move( endcapstub4y ),                  prefix + "EndcapStub4Y"                  + suffix );
    iEvent.put( move( endcapstub4z ),                  prefix + "EndcapStub4Z"                  + suffix );
    iEvent.put( move( endcapstub4eta ),                prefix + "EndcapStub4Eta"                + suffix );
    iEvent.put( move( endcapstub4phi ),                prefix + "EndcapStub4Phi"                + suffix );
    iEvent.put( move( endcapstub5x ),                  prefix + "EndcapStub5X"                  + suffix );
    iEvent.put( move( endcapstub5y ),                  prefix + "EndcapStub5Y"                  + suffix );
    iEvent.put( move( endcapstub5z ),                  prefix + "EndcapStub5Z"                  + suffix );
    iEvent.put( move( endcapstub5eta ),                prefix + "EndcapStub5Eta"                + suffix );
    iEvent.put( move( endcapstub5phi ),                prefix + "EndcapStub5Phi"                + suffix );
    iEvent.put( move( endcapstub6x ),                  prefix + "EndcapStub6X"                  + suffix );
    iEvent.put( move( endcapstub6y ),                  prefix + "EndcapStub6Y"                  + suffix );
    iEvent.put( move( endcapstub6z ),                  prefix + "EndcapStub6Z"                  + suffix );
    iEvent.put( move( endcapstub6eta ),                prefix + "EndcapStub6Eta"                + suffix );
    iEvent.put( move( endcapstub6phi ),                prefix + "EndcapStub6Phi"                + suffix );
  }

}
